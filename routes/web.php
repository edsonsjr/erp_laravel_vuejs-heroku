<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();


Route::get('/crm', 'CrmController@index')->name('crm');
Route::get('/home', 'HomeController@index');


Route::middleware('role:admin')->prefix('admin')->namespace('Admin')->group(function(){
    Route::resource('roles', 'RoleController');
    Route::resource('usuario', 'UsuarioController');
    Route::get('/usuario-role/{idUsuario}', 'UsuarioRoleController@buscaRolesUsuario');
    Route::put('/usuario-role/{idUsuario}', 'UsuarioRoleController@buscaRolesUsuario');
    
});


Route::middleware('auth')->prefix('admin')->namespace('Admin')->group(function(){
    Route::resource('empresa', 'EmpresaController');
});

Route::middleware('auth')->prefix('user')->namespace('User')->group(function(){
    Route::resource('estoque', 'EstoqueController');
    Route::resource('chip', 'ChipController');
    Route::resource('equipamento', 'EquipamentoController');
    Route::resource('fornecedor', 'FornecedorController');
    Route::get('busca-chip/{id}', 'KitController@buscaChip');
    Route::get('busca-equip/{id}', 'KitController@buscaEquip');
    Route::resource('kit', 'KitController');
    Route::resource('produto', 'ProdutoController');
    Route::resource('movimento', 'MovimentoController');
    Route::get('limpa/{key}', 'MovimentoController@clearSessionKey');
});
