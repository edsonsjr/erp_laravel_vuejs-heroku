<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>{{ config('app.name', 'Ranor') }}</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet" type="text/css">
        <link href="https://unpkg.com/ionicons@4.5.5/dist/css/ionicons.min.css" rel="stylesheet">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.1/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">

        <!-- Styles -->
        <style>
            body {
                background-image: url("https://media.wired.com/photos/59bafdf204afdc5248726f5c/master/w_2400,c_limit/BMW-TA.jpg");
                background-color: #cccccc;
                background-position: center; 
                background-repeat: no-repeat; 
                background-size: cover;
                background-position-y: 3px;
                height: 100%;
            }

            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 13px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }

            .header-info {
                font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Oxygen, Ubuntu, Cantarell, 'Open Sans', 'Helvetica Neue', sans-serif;
                color: #ffffff;
                width: 100%;
                height: 20%;
                padding-top: 20px;
                
            }

            .header-info p {
                text-align: center;
                text-shadow: 1px 1px #000000;
            }

            .actions-list {
                text-align: right;
            }

            .actions-list li{
                list-style: none;
                display: inline;
                margin-right: 30px;
            }

            .user-photo {
                font-size: 40px;
            }

            .main-content {
                width: 500px;
                margin: 0 auto;
            }


            .modules-list {
                display: flex;
                justify-content: space-around;
                padding: 0;
                list-style: none;
                
                
            }
            .modules-list li {
                display: inline-block;
                color:white;
                /*background-color: #b91b1b;*/
            }
            .modules-list i {
                margin-right: 5px; 
            }

            .first-row {
                width: 150px;
                height: 100px;
                text-align: center;
                padding-top: 60px;
            }

            .second-row {
                width: 235px;
                height: 100px;
                text-align: center;
                padding-top: 60px;
            }

            .first-row.first-item {
                background-color: #5d24a8;
            }
            .first-row.second-item {
                background-color: #e4871e;
            }
            .first-row.third-item {
                background-color: #25c4cf;
            }
            .second-row.first-item {
                background-color: #b91b1b;
            }
            .second-row.second-item {
                background-color: #30db2a;
            }

        </style>
    </head>
    <body>
        <div class="flex-center position-ref full-height">
            @if (Route::has('login'))
                <div class="top-right links">
                    @auth
                        Bem vindo, {{ Auth::user()->name }} !
                        <a href="{{ url('/home') }}">Home</a>
                        <a href="{{ route('logout') }}" onclick="event.preventDefault();document.getElementById('logout-form').submit();"><i class="fas fa-power-off"></i></a>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            @csrf
                        </form>
                    @else
                        <a href="{{ route('login') }}">Login</a>

                        @if (Route::has('register'))
                            <a href="{{ route('register') }}">Register</a>
                        @endif
                    @endauth
                </div>
            @endif

            <div class="content">
                <div class="title m-b-md">
                    {{ config('app.name', 'Ranor') }}
                </div>
                <div class="main-content">
                    <ul class="modules-list first-list">
                        <a href="{{ url('/crm') }}"><li class="first-row first-item"><i class="fas fa-angle-double-down"></i><p>CRM</p></li></a>
                        <a href=""><li class="first-row second-item"><i class="fas fa-chart-line"></i><p>Vendas e NF-e</p></li></a>
                        <a href=""><li class="first-row third-item"><i class="fas fa-list-ul"></i><p>Serviços e NFS-e</p></li></a>
                    </ul>
                    <ul class="modules-list second-list">
                        <a href="{{ url('/user/estoque')}}"><li class="second-row first-item"><i class="fas fa-boxes"></i><p>Compras Estoque e Produção</p></li></a>
                        <a href="{{ url('/admin/empresa')}}"><li class="second-row second-item"><i class="fas fa-info-circle"></i><p>Dados da Empresa</p></li></a>
                    </ul>
                </div> 
            </div>
        </div>
    </body>
</html>
