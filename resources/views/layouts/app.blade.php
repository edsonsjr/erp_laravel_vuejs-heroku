<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Ranor') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>
 
    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.1/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
    <!--<link rel="stylesheet" href="{{ asset('css/all.css') }}" >-->

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body>
    <div id="app">
        
        @if (Auth::check())
            <mega-menu  url="{{ route('logout') }}" token="{{ csrf_token() }}" empresa="{{ config('app.name', 'Ranor') }}">
                <span slot="csrf">@csrf</span>
                <mega-menu-item-empresa icone="fas fa-building" categoria="Empresa"></mega-menu-item-empresa>
                <mega-menu-item icone="fas fa-dollar-sign" categoria="Financeiro"></mega-menu-item>
                @role('admin')
                <mega-menu-item-admin url="{{ url('/admin/') }}" icone="fas fa-users-cog" categoria="Administrador"></mega-menu-item-admin></span>
                @endrole

                @hasanyrole('admin|support')
                <mega-menu-item-estoque  url="{{ url('/user/') }}" icone="fas fa-boxes" categoria="Estoque"></mega-menu-item>
                @endhasanyrole
               
                
            </mega-menu>
        @endif
        
        @yield('content')

    </div>

    <script>
       
    </script>
</body>
</html>
