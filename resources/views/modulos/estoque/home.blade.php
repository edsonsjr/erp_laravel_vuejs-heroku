@extends('layouts.app')

@section('content')
<div >
    <caminho-migalhas v-bind:list="{{$listaMigalhas}}"></caminho-migalhas>  
    
    <div class="row mr-3 ml-3">
        <div class="col-md-auto">
            <a href="{{ route('produto.index')}}" class="btn btn-success">Gerenciar Produtos</a>
        </div>
        <div class="col-md-auto">
            <a href="{{ route('fornecedor.index')}}" class="btn btn-success">Gerenciar Fornecedores</a>
        </div>
        <div class="col-md-auto">
            <a href="{{ route('movimento.index')}}" class="btn btn-success">Movimentar Estoque</a>
        </div>
       
    </div>
</div>
@endsection
