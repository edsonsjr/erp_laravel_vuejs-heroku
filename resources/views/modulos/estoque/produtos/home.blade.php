@extends('layouts.app')

@section('content')
<div >
    <caminho-migalhas v-bind:list="{{$listaMigalhas}}"></caminho-migalhas>  
    <div class="row ml-3 mr-3">
        <div class="col-md-12">
            @if($errors->all())
                <div class="alert alert-danger alert-dismissible text-center" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                    @foreach( $errors->all() as $key => $value)
                    <li><strong>{{$value}}</strong> </li>
                    @endforeach
                </div>
            @endif
            @if(isset($produto))
            <div class="alert alert-success alert-dismissible fade show" role="alert">
                Produto <strong>{{$produto->pro_dsc}}</strong> cadastrado com sucesso.
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            @endif
        </div>
    </div>
    
    <div class="row mr-3 ml-3">
        <div class="col-md-8">
            <div class="card">
                <div class="container">
                    <painel-produtos nome="Listagem de Produtos">
                        <tabela-produto url="{{ route('produto.index') }}" token="{{ csrf_token() }}" v-bind:itens="{{$produtosQ}}"></tabela-produto>
                    </painel-produtos>
                </div>
                
            </div>
        </div>
        <div class="col-md-4">
            <div class="card">
                <div class="container">
                    <formulario-produto url="{{ route('produto.store')}}" caminho-imagem="{{ url('/img/icone_produto.png')}}"  token="{{ csrf_token() }}"></formulario-produto>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
