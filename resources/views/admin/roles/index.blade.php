@extends('layouts.app')


@section('content')
<div >
    <caminho-migalhas v-bind:list="{{$listaMigalhas}}"></caminho-migalhas>  
    <div class="row ml-3 mr-3">
        <div class="col-md-12">
            @if($errors->all())
                <div class="alert alert-danger alert-dismissible text-center" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                    @foreach( $errors->all() as $key => $value)
                    <li><strong>{{$value}}</strong> </li>
                    @endforeach
                </div>
            @endif
        </div>
    </div>
    <div class="row mr-3 ml-3">
        <div class="col-md-8">
            <div class="card">
                <div class="container">
                    <painel-produtos nome="Listagem de Funções e Permissões">
                        <tabela-roles url="{{ route('roles.index') }}" token="{{ csrf_token() }}" v-bind:itens="{{$roles->toJson()}}"></tabela-roles>
                    </painel-produtos>
                </div>
                
            </div>
        </div>
        <div class="col-md-4">
            <div class="card">
                <div class="container">
                    <permissoes-role></permissoes-role>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection