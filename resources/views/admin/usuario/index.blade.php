@extends('layouts.app')

@section('content')
<div>
    <caminho-migalhas v-bind:list="{{$listaMigalhas}}"></caminho-migalhas>
    <div class="row ml-3 mr-3">
        <div class="col-md-12">
            @if($errors->all())
                <div class="alert alert-danger alert-dismissible text-center" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                    @foreach( $errors->all() as $key => $value)
                    <li><strong>{{$value}}</strong> </li>
                    @endforeach
                </div>
            @endif

        </div>
    </div>
    <div class="row mr-3 ml-3">
        <div class="col-md-8">
            <painel-geral nome="Listagem de Usuarios">
                <tabela-usuario id-usuario="{{auth()->user()->id}}" url-roles="{{ url('admin/usuario-role/')}}" v-bind:itens="{{$usuarios}}"></tabela-usuario>
            </painel-geral>
        </div>
        <div class="col-md-4">
            <div class="row">
                    <formulario-usuario  
                    url="{{ route('usuario.store')}}" 
                    v-bind:roles="{{$roles}}" 
                    v-bind:roles-usuario="{{$rolesUsuario}}"  
                    token="{{csrf_token()}}"></formulario-usuario>
                
            </div>
            <div class="row pt-2">
                    <funcoes-usuario  
                    url-roles="{{ url('admin/usuario-role/')}}"
                    url-user="{{ route('usuario.store') }}"
                    v-bind:roles="{{$roles}}" 
                    v-bind:roles-usuario="{{$rolesUsuario}}" 
                    token="{{csrf_token()}}">
                    </funcoes-usuario>  
                
            </div>
            
        </div>
        
    </div>
</div>
@endsection
