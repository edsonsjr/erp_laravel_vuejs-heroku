import Vuex from 'Vuex';
import Vue from 'vue';

Vue.use(Vuex);

const store = new Vuex.Store({

    state: {
        chip: {},
        equip: {},
        lote_info: {},
        lote_chips: [],
        lote_produtos: [],
        produto: {},
        obj: {},
        objs: {},
        roles: {},
        produtos: {},
        infoFormUpdate: {},
        role: {},
        roles: {},
        perm: {},
        perms: {},
        movimento: [],
        fornecedor: {},
        fornecedores: {},
    },
    mutations:{
        setObj(state, obj){
            state.obj = obj;
        },
        setObjs(state, obj){
            state.objs = obj;
        },
        setRoles(state, obj){
            state.roles = obj;
        },
        setChip(state, obj){
            state.chip = obj;
        },
        setProduto(state, obj){
            state.produto = obj;
        },
        setInfoLote(state, obj){
            state.lote_info = obj;
        },
        setChipsLote(state, obj){
            state.lote_chips.push(obj);
        },
        setProdutosLote(state, obj){
            state.lote_produtos.push(obj);
        },
        setProdutosLimpo(state, obj){
            state.lote_produtos = obj;
        },
        setInfoForm(state, obj){
            state.infoFormUpdate = obj;
        },
        setProdutos(state, obj){
            state.produtos = obj;
        },
        setEquip(state, obj){
            state.equip = obj;
        },
        setRole(state, obj){
            state.role = obj;
        },
        setPerms(state, obj){
            state.perms = obj;
        },
        setPerm(state, obj){
            state.perm = obj;
        },
        setMovimento(state, obj){
            state.movimento = obj;
        },
        setPropMovimento(state, obj){
            state.movimento.push(obj);
        },
        setFornecedor(state, obj){
            state.fornecedor = obj;
        },
        setFornecedores(state, obj){
            state.fornecedores = obj;
        },
    }
});


export default store;