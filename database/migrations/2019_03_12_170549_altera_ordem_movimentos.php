<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlteraOrdemMovimentos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('movimentos', function (Blueprint $table) {
            //
            $table->renameColumn('mov_numkit', 'mov_numkit_old');

        });
        Schema::table('movimentos', function (Blueprint $table) {
            //
            $table->string('mov_numkit')->before('mov_sn')->nullable();

        });

        DB::table('movimentos')->update([
            'mov_numkit' => DB::raw('mov_numkit_old')   
        ]);
    
         //Remove the old column:
        Schema::table('movimentos', function(Blueprint $table)
        {
            $table->dropColumn('mov_numkit_old');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('movimentos', function (Blueprint $table) {
            //
        });
    }
}
