<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AdicionaColunasEnderecoFornecedores extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('fornecedores', function (Blueprint $table) {
            //
            $table->dropColumn('for_end');
            $table->string('for_rua');
            $table->string('for_numero');
            $table->string('for_bairro');
            $table->string('for_complemento');
            $table->string('for_estado');
            $table->string('for_cidade');
            $table->string('for_cep');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('fornecedores', function (Blueprint $table) {
            //
        });
    }
}
