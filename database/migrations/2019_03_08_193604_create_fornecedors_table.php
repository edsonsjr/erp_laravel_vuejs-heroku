<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFornecedorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fornecedors', function (Blueprint $table) {
            $table->increments('id');
            $table->string('for_razsoc');
            $table->string('for_nome');
            $table->string('for_cpfcnpj');
            $table->string('for_tel');
            $table->string('for_cont');
            $table->string('for_end');
            $table->string('for_telsec')->nullable();
            $table->string('for_fax')->nullable();
            $table->string('for_email')->nullable();
            $table->string('for_site')->nullable();
            $table->string('for_inscest')->nullable();
            $table->string('for_inscmun')->nullable();
            $table->string('for_inscsuf')->nullable();
            $table->string('for_tipoatv')->nullable();
            $table->string('for_cnae')->nullable();
            $table->boolean('for_simp')->nullable();
            $table->boolean('for_rural')->nullable();
            $table->string('for_obs')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fornecedors');
    }
}
