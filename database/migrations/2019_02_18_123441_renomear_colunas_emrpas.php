<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RenomearColunasEmrpas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('empresas', function (Blueprint $table) {
            $table->renameColumn('razao_social_empresa', 'emp_razao_social');
            $table->renameColumn('nome_fantasia_empresa', 'emp_nome_fantasia');
            $table->renameColumn('cnpj_empresa', 'emp_cnpj');
            $table->renameColumn('telefone_empresa', 'emp_tel');
            $table->renameColumn('nome_contato_empresa', 'emp_contato');
            $table->string('emp_email');
            $table->string('emp_website');
            $table->string('emp_tel_sec');
            $table->string('emp_fax');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('empresas', function (Blueprint $table) {
            //
        });
    }
}
