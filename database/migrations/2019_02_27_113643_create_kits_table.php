<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateKitsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kits', function (Blueprint $table) {
            $table->increments('id');
            $table->string('kit_num');
            $table->string('kit_nome');
            $table->integer('id_chip');
            $table->integer('id_equip');
            $table->timestamps();


            $table->foreign('id_chip')
                ->references('id')->on('chips')
                ->onDelete('cascade');

            $table->foreign('id_equip')
                ->references('id')->on('equipamentos')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('kits');
    }
}
