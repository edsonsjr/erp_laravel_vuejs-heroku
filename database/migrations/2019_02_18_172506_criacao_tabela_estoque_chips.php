<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CriacaoTabelaEstoqueChips extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('chips', function (Blueprint $table) {
            $table->increments('id');
            $table->string('chip_status');
            $table->string('chip_op');
            $table->string('chip_iccid');
            $table->string('chip_num');
            $table->date('chip_data_ativ');
            $table->integer('chip_mb');
            $table->double('chip_mens', 8, 2);
            $table->date('chip_data_venc');
            $table->string('chip_for');
            $table->timestamps();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('chips');
    }
}
