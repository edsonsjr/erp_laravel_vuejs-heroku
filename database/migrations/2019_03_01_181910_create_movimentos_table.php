<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMovimentosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('movimentos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('mov_sn')->nullable();
            $table->string('mov_iccid')->nullable();
            $table->string('mov_numchip')->nullable();
            $table->string('mov_sit');
            $table->string('mov_obs')->nullable();
            $table->boolean('mov_novo')->default(1);
            $table->boolean('mov_tipo');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('movimentos');
    }
}
