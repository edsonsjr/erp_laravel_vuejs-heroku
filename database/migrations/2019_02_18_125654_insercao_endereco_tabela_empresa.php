<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class InsercaoEnderecoTabelaEmpresa extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('empresas', function (Blueprint $table) {
            $table->string('emp_rua');
            $table->integer('emp_numero');
            $table->string('emp_complemento');
            $table->string('emp_bairro');
            $table->string('emp_estado');
            $table->string('emp_cidade');
            $table->string('emp_cep');
            $table->dropColumn('endereco_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('empresas', function (Blueprint $table) {
            
        });
    }
}
