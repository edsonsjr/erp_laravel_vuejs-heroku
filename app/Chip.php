<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Chip extends Model
{
    protected $fillable = [
        'chip_status', 'chip_op', 'chip_iccid', 'chip_num', 'chip_mb',
        'chip_mens', 'chip_dia_venc','chip_data_ativ', 'chip_for'
    ];
}
