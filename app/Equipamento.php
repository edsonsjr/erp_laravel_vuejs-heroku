<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Equipamento extends Model
{
    //
    protected $fillable = [
        'equip_imei', 'equip_num', 'equip_mod', 'equip_data_inc',
        'equip_valor', 'equip_data_comp', 'equip_for'
    ];
}
