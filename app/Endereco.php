<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Endereco extends Model
{
    //
    protected $fillable = [
        'endereco', 'numero', 'complemento', 'bairro', 'estado', 'cidade', 'cep',
    ];
}
