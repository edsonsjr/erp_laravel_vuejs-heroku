<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\DB;
use App\Produto;

class AvisoEstoqueMin extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    protected $produtos;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($produtos)
    {
        //
        $this->produtos = $produtos;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.producao.estoquemin')->with(['produtos' => $this->produtos]);
    }

   
}
