<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Produto;

class Fornecedor extends Model
{
    //
    protected $fillable = ['for_razsoc', 'for_nome', 'for_cpfcnpj', 'for_tel', 'for_cont', 'for_end', 'for_telsec'
    ,'for_fax', 'for_email', 'for_site', 'for_inscest' , 'for_inscmun', 'for_inscsuf', 'for_tipoatv', 'for_cnae',
    'for_simp', 'for_rural', 'for_rua', 'for_numero', 'for_bairro', 'for_complemento', 'for_estado', 'for_cidade',
    'for_cep'];

    protected $table = 'fornecedores';

    protected $hidden = ['created_at', 'updated_at'];

    public function produtos()
    {
        return $this->hasMany(Produto::class);
    }
}
