<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Empresa extends Model
{
    protected $fillable = [
        'emp_razao_social', 'emp_nome_fantasia', 'emp_cnpj', 'emp_tel', 'emp_contato', 'emp_email', 
        'emp_website', 'emp_tel_sec', 'emp_fax', 'emp_rua', 'emp_numero', 'emp_complemento', 'emp_bairro',
        'emp_estado', 'emp_cidade', 'emp_cep',
    ];
}
