<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Produto extends Model
{
    //
    protected $fillable = ['pro_dsc', 'pro_img', 'pro_cod', 'pro_min','pro_val_ven', 'pro_uni', 'pro_tipo'];

    protected $hidden = [ 'pro_img'];

    public function movimentos()
    {
        return $this->hasMany('App\Movimento');
    }
}
