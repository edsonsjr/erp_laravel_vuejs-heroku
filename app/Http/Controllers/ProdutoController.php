<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Produto;

class ProdutoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //

        // Array que fornece informações para geração do Breadcrumb
        $listaMigalhas = json_encode([
            ['titulo' => 'Home', 'url' => '/'],
            ['titulo' => 'Estoque', 'url' => '/user/estoque'],
            ['titulo' => 'Cadastro de Produtos', 'url' => '']
        ]);

        // Array que armazena os produtos juntamente com suas quantidades em estoque
        $produtosQ = [];


        //  Produtos cadastrados no banco 
        $produtosSQ = Produto::all();
        
        

        // Itera sobre os produtos cadastrados no banco
        foreach($produtosSQ as $prod)
        {
            // Captura o id do produto
            $id = $prod['id'];
            // Chamada de função do BD que calcula a quantidade de estoque do produto informado
            $qtd = DB::select('SELECT public."quantidadeestoque"('.$id.')');
            // Adiciona a quantidade ao produto
            $prod->qtd = $qtd[0]->quantidadeestoque;
            // Adiciona o produto ao array que será retornada a view
            array_push($produtosQ, $prod);
        }

        // Encode JSON

        $produtosQ = json_encode($produtosQ);
        
        
        
        return view('modulos/estoque/produtos/home',compact('listaMigalhas', 'produtosQ'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // Array que fornece informações para geração do Breadcrumb
         $listaMigalhas = json_encode([
            ['titulo' => 'Home', 'url' => '/'],
            ['titulo' => 'Estoque', 'url' => '/user/estoque'],
            ['titulo' => 'Cadastro de Produtos', 'url' => '']
        ]);
        //
        $data = $request->all();
        $produto = Produto::create($data);

        //  Produtos cadastrados no banco 
        $produtosSQ = Produto::all();
        
        // Array que armazena os produtos juntamente com suas quantidades em estoque
        $produtosQ = [];

        // Itera sobre os produtos cadastrados no banco
        foreach($produtosSQ as $prod)
        {
            // Captura o id do produto
            $id = $prod['id'];
            // Chamada de função do BD que calcula a quantidade de estoque do produto informado
            $qtd = DB::select('SELECT public."quantidadeestoque"('.$id.')');
            // Adiciona a quantidade ao produto
            $prod->qtd = $qtd[0]->quantidadeestoque;
            // Adiciona o produto ao array que será retornada a view
            array_push($produtosQ, $prod);
        }

        // Encode JSON

        $produtosQ = json_encode($produtosQ);
        
        return view('modulos/estoque/produtos/home', compact('listaMigalhas', 'produto', 'produtosQ'));

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
