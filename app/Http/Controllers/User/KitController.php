<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Chip;
use App\Equipamento;
use App\Kit;

class KitController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // Busca os kits cadastrados juntamente com informações do chip e do equipamento a ele atrelados
        $kits = DB::table('kits')
                    ->join('chips', 'kits.id_chip', '=', 'chips.id')
                    ->join('equipamentos', 'kits.id_equip', '=', 'equipamentos.id')
                    ->select('kits.*', 'chips.chip_iccid', 'equipamentos.equip_imei')
                    ->get();


        // Busca os ids dos chips e dos equipamentos que estao atralados a um kit
        $chipsInKits = Kit::select('id_chip')->where('id_chip' ,'>' ,0)->pluck('id_chip')->toArray();
        $equipsInKits = Kit::select('id_equip')->where('id_equip' ,'>' ,0)->pluck('id_equip')->toArray();

        // Busca no banco quais os chips e equipamentos cadastrados
        $chipsCadastrados = Chip::where('id' ,'>' ,0)->pluck('id')->toArray();
        $equipsCadastrados = Equipamento::where('id' ,'>' ,0)->pluck('id')->toArray();

        // Array que ira armazenar os chips que não estão atrelados a um Kit
        $chipsDisponiveis = array();

        // Itera sobre o array dos chips cadastrados e armazena no array de chip disponiveis
        // Apenas aqueles que não estão atrelados a um Kit
        foreach($chipsCadastrados as $chip)
        {
            if(!(in_array($chip, $chipsInKits)))
            {
                array_push($chipsDisponiveis, $chip);
            }
        }

        // Array que ira armazenar os equipamentos que não estão atrelados a um Kit
        $equipsDisponiveis = array();

        
        // Itera sobre o array dos chips cadastrados e armazena no array de chip disponiveis
        // Apenas aqueles que não estão atrelados a um Kit
        foreach($equipsCadastrados as $equip)
        {
            if(!(in_array($equip, $equipsInKits)))
            {
                array_push($equipsDisponiveis, $equip);
            }
        }
        
        // Transforma array em JSON

        $chipsD = json_encode($chipsDisponiveis);
        $equipsD = json_encode($equipsDisponiveis);

         // Array que fornece informações para geração do Breadcrumb
        $listaMigalhas = json_encode([
            ['titulo' => 'Home', 'url' => '/'],
            ['titulo' => 'Estoque', 'url' => '/user/estoque'],
            ['titulo' => 'Kits', 'url' => '']
        ]);

        
    

        // Retorna para a view os chips,equipamentos disponíveis e kits cadastrados

        return view('modulos/estoque/kit/home', compact('kits', 'chipsD', 'equipsD', 'listaMigalhas'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        // Teste
        $data = $request->all();
        $nextId = DB::table('kits')->max('id') + 1;
        $numCasas = round($nextId / 10)+1;
        $kit_num = '075550318';
        for($i=0; $i<=(7-$numCasas); $i++)
        {
            $kit_num .= '0';
        }
        $kit_num .= $nextId;
        $kit = [
            "id_chip" => $data['id_chip'],
            "id_equip"=> $data['id_equip'],
            "kit_num" => $kit_num,
        ];

        Kit::create($kit);

        
        // Busca os kits cadastrados juntamente com informações do chip e do equipamento a ele atrelados
        $kits = DB::table('kits')
                    ->join('chips', 'kits.id_chip', '=', 'chips.id')
                    ->join('equipamentos', 'kits.id_equip', '=', 'equipamentos.id')
                    ->select('kits.*', 'chips.chip_iccid', 'equipamentos.equip_imei')
                    ->get();

        return $kits;

    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // Identifica se o numero é de um equipamento ou de um chip

        // Busca no banco se o chip existe ou não
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     *  Função responsável por buscar no banco pelo
     *  chip com Iccid passado como parâmetro
     * 
     * @param Int iccid
     * @return Int iccid se encontrado e -1 senão.
     */
    public function buscaChip($iccid)
    {
        $chip = Chip::where('chip_iccid', '=', $iccid)->get();
        $kit = Kit::where('id_chip', '=', $chip[0]->id)->get();
        if(!($chip->count() > 0)) return -1;
        if($kit->count() > 0) return -2;
        return json_encode($chip);
    }
    /**
     *  Função responsável por buscar no banco pelo
     *  equipamento com Imei passado como parâmetro
     * 
     * @param Int Imei
     * @return Int Imei se encontrado e -1 senão.
     */
    public function buscaEquip($imei)
    {
        $equip = Equipamento::where('equip_imei', '=', $imei)->get();
        $kit = Kit::where('id_equip', '=', $equip[0]->id)->get();
        if(!($equip->count() > 0)) return -1;
        if($kit->count() > 0) return -2;
        return json_encode($equip);
    }
}
