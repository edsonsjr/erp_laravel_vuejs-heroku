<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Equipamento;
use App\User;

class EquipamentoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
        

     
        $listaEquipamentos = json_encode(Equipamento::select('id', 'equip_imei', 'equip_num', 'equip_mod', 
        'equip_data_comp', 'equip_data_inc', 'equip_for', 'equip_valor')->get());



        // Array que fornece informações para geração do Breadcrumb
        $listaMigalhas = json_encode([
            ['titulo' => 'Home', 'url' => '/'],
            ['titulo' => 'Estoque', 'url' => '/user/estoque'],
            ['titulo' => 'Equipamentos', 'url' => '']
        ]);


        return view('modulos/estoque/equipamento/home',compact('listaMigalhas', 'listaEquipamentos'));
    }



    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $data = $request->all(); // Coleta todos os dados da requisição
        /*
        $rules = [
            "chips_status" => "required",
            "chip_op" => "required",
            "chip_iccid" => "required|numeric",
            "chip_num" => "required|numeric",
            "chip_data_ativ" => "required|date",
            "chip_mb" => "required|numric",
            "chip_mens" => "required|numeric",
            "chip_dia_venc" => "required|numeric",
            "chip_for" => "required"
        ];
        // Validação

        $validation = \Validator::make($data, $rules);

        if($validation->fails()) {
            return redirect('user/chip/')->withErrors($validation)->withInput();
        }*/
        Equipamento::create($data);
        return redirect('user/equipamento/');

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Chip  $chip
     * @return \Illuminate\Http\Response
     */
    public function show(Equipamento $equipamento)
    {
        $listaChips = json_encode(Equipamento::select('id', 'chip_status', 'chip_op', 'chip_iccid', 
        'chip_num', 'chip_mb', 'chip_mens', 'chip_for', 'chip_dia_venc', 'chip_data_ativ')->get());
        return $listaChips;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Chip  $chip
     * @return \Illuminate\Http\Response
     */
    public function edit(Equipamento $equipamento)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Chip  $chip
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Equipamento $equipamento)
    {
        //
       
        $data = $request->all();
        Equipamento::find($data['id'])->update($data);
        $listaEquipamentos = json_encode(Equipamento::select('id', 'equip_imei', 'equip_num', 'equip_mod', 
        'equip_data_comp', 'equip_data_inc', 'equip_for', 'equip_valor')->get());
        return $listaEquipamentos;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Chip  $chip
     * @return \Illuminate\Http\Response
     */
    public function destroy(Equipamento $equipamento)
    {
        $equipamentoDeletado = Equipamento::findOrFail($equipamento['id']);
        $equipamentoDeletado->delete();
        $listaEquipamentos = json_encode(Equipamento::select('id', 'equip_imei', 'equip_num', 'equip_mod', 
        'equip_data_comp', 'equip_data_inc', 'equip_for', 'equip_valor')->get());
        return $listaEquipamentos;
    }
}
