<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Chip;


class ChipController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        


        $listaChips = json_encode(Chip::select('id', 'chip_status', 'chip_op', 'chip_iccid', 
        'chip_num', 'chip_mb', 'chip_mens', 'chip_for', 'chip_dia_venc', 'chip_data_ativ')->get());


        // Array que fornece informações para geração do Breadcrumb
        $listaMigalhas = json_encode([
            ['titulo' => 'Home', 'url' => '/'],
            ['titulo' => 'Estoque', 'url' => '/user/estoque'],
            ['titulo' => 'Chips', 'url' => '']
        ]);


        return view('modulos/estoque/chip/home',compact('listaMigalhas', 'listaChips'));
    }



    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $data = $request->all(); // Coleta todos os dados da requisição

        // Coleta as chaves do array
        $keys = array_keys($data);

        // Verifica se a primeira chave do array é 0, caso afirmativo 
        // serão cadastrados multiplos chips
        if($keys[0] == '0'){
            // Itera sobre os dados coletados da requisicao para inserir no banco
            foreach($data as $chip){
                Chip::create($chip);
            }
        }
        
        // Senao cadastra um unico chip  

        $rules = [
            "chips_status" => "required",
            "chip_op" => "required",
            "chip_iccid" => "required|numeric",
            "chip_data_ativ" => "required|date",
            "chip_mb" => "required|numric",
            "chip_mens" => "required|numeric",
            "chip_dia_venc" => "required|numeric",
            "chip_for" => "required"
        ];
        // Validação

        $validation = \Validator::make($data, $rules);

        if($validation->fails()) {
            return redirect('user/chip/')->withErrors($validation)->withInput();
        }
        Chip::create($data);
        return redirect('user/chip/');

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Chip  $chip
     * @return \Illuminate\Http\Response
     */
    public function show(Chip $chip)
    {
        $listaChips = json_encode(Chip::select('id', 'chip_status', 'chip_op', 'chip_iccid', 
        'chip_num', 'chip_mb', 'chip_mens', 'chip_for', 'chip_dia_venc', 'chip_data_ativ')->get());
        return $listaChips;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Chip  $chip
     * @return \Illuminate\Http\Response
     */
    public function edit(Chip $chip)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Chip  $chip
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Chip $chip)
    {
        //
       
        $data = $request->all();
        Chip::find($data['id'])->update($data);
        $chipUp = Chip::find($data['id']);
        $listaChips = json_encode(Chip::select('id', 'chip_status', 'chip_op', 'chip_iccid', 
        'chip_num', 'chip_mb', 'chip_mens', 'chip_for', 'chip_dia_venc', 'chip_data_ativ')->get());
        return $listaChips;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Chip  $chip
     * @return \Illuminate\Http\Response
     */
    public function destroy(Chip $chip)
    {
        $chipDeletado = Chip::findOrFail($chip['id']);
        $chipDeletado->delete();
        $listaChips = json_encode(Chip::select('id', 'chip_status', 'chip_op', 'chip_iccid', 
        'chip_num', 'chip_mb', 'chip_mens', 'chip_for', 'chip_dia_venc', 'chip_data_ativ')->get());
        return $listaChips;
    }
}
