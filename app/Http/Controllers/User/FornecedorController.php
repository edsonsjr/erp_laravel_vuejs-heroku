<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Fornecedor;

class FornecedorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
        // Array que fornece informações para geração do Breadcrumb
        $listaMigalhas = json_encode([
            ['titulo' => 'Home', 'url' => '/'],
            ['titulo' => 'Estoque', 'url' => '/user/estoque'],
            ['titulo' => 'Fornecedores', 'url' => '']
        ]);

        // Busca fornecedores no banco
        $listaFornecedores = json_encode(Fornecedor::all());

        return view('modulos/estoque/fornecedor/home',compact('listaMigalhas', 'listaFornecedores'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // Resgata os dados do fornecedor a ser cadastrado
        $data = $request->all();

        //dd($data);
        // Cria o fornecedor
        Fornecedor::create($data);

        // Mensagem de callback
        $msg = $data['for_razsoc'];

        return redirect()->back()->with('fornecedor', $msg);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // Dados da requisicao
        $data = $request->all();

        // Busca o fornecedor com o id passado por parametro na url
        Fornecedor::find($id)->update($data);

        // Busca fornecedores no banco
        $listaFornecedores = json_encode(Fornecedor::all());

        return $listaFornecedores;

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // Busca o fornecedor a ser deletado
        $fornecedorDeletado = Fornecedor::findOrFail($id);

        // Chamada da ação de deleção
        $fornecedorDeletado->delete();

        // Lista dos fornecedores apos exclusao
        $listaFornecedores = Fornecedor::all();

        return $listaFornecedores;
    }
}
