<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use App\Http\Controllers\Controller;
use App\Produto;
use App\Movimento;
use App\Fornecedor;
use App\Mail\AvisoEstoqueMin;

class MovimentoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

    




        // Array que fornece informações para geração do Breadcrumb
        $listaMigalhas = json_encode([
            ['titulo' => 'Home', 'url' => '/'],
            ['titulo' => 'Estoque', 'url' => '/user/estoque'],
            ['titulo' => 'Movimentação de Estoque', 'url' => '']
        ]);

        // Busca no banco os produtos cadastrados
        $listaProdutos = json_encode(Produto::all());

        // Busca no banco os fornecedores cadastrados
        $listaFornecedores = json_encode(Fornecedor::all());

        // Busca no banco os movimentos
        $listaMovimentosC = DB::table('movimentos')->join('fornecedores', 'movimentos.mov_for', '=', 'fornecedores.id')
        ->join('produtos', 'movimentos.mov_pro', '=', 'produtos.id')
        ->select('movimentos.id', 'produtos.pro_dsc','movimentos.mov_numkit','movimentos.mov_sn', 'movimentos.mov_iccid','movimentos.mov_numchip', 'movimentos.mov_sit', 'fornecedores.for_nome','movimentos.mov_obs', 'movimentos.mov_novo', 'movimentos.mov_tipo', 'movimentos.mov_mb'
        , 'movimentos.mov_diavenc', 'movimentos.mov_mens', 'movimentos.mov_val')->simplePaginate(10);
        $listaMovimentos = json_encode($listaMovimentosC);
        
        //dd($listaMovimentosC);
        
        //Funcao que verifica o estoque minino do produto
        //$this->verificaEstoqueMin();

        if(session('visualizada') == 0)
        {
            session(['visualizada' => 1]);
            return view('modulos/estoque/movimento/home', compact('listaMigalhas', 'listaProdutos', 'listaMovimentos', 'listaMovimentosC', 'listaFornecedores'));
        }
        if(session('visualizada') == 1)
        {
            session(['validacao' => '','visualizada' => 1]);
            return view('modulos/estoque/movimento/home', compact('listaMigalhas', 'listaProdutos', 'listaMovimentos', 'listaMovimentosC', 'listaFornecedores'));
        }


        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // Dados da requisição
        $data = $request->all();

        // Var que armazena msg de validacao
        $validacao = '';


        // Verifica se os dados enviados estao em lote ou valor unico
        if(array_key_exists('0', $data))
        {
            // Itera sobre os dados repassados em lote
            foreach($data as $produto)
            {
                // Armazena a mensagem da verifica do processo
                $validacao .= $this->verificaES($produto).' / ';
            }

            // Armazena na sessao a mensagem de validacao
            session(['validacao' => $validacao, 'visualizada' => 0]);

            // Retorna a url para o axios
            return url('user/movimento');
        }
        // Se for um valor único
        if(!(array_key_exists('0', $data)))
        {

    
            // Verifica se o produto não possui identificação unica
            // É do tipo acessório
            // ID do produto
            //dd($data);
            $id = $data['mov_pro'];

           
            // Produto do movimento
            $produto = Produto::find($id);
            
            // Se o produto for do tipo acessório
            if($produto['pro_tipo'] == 'ace')
            {
                $numMovimento = $data['mov_numitens'];

                for($i = 0; $i<$numMovimento; $i++)
                {
                    $validacao .= $this->verificaES($data).' / ';
                }

                // Verificar se veio do axios
                if(array_key_exists('mov_ori', $data)){
                    
                    if($data['mov_ori'] == 'axios') {
                        // Armazena na sessao a mensagem de validacao
                        session(['validacao' => $validacao, 'visualizada' => 0]);

                        // Retorna a url para o axios
                        return url('user/movimento');
                    }
                    
                }
                
                session(['visualizada' => 0]);
                return redirect()->back()->with('validacao', $validacao);
            }

            
            $validacao = $this->verificaES($data);
            session(['visualizada' => 0]);
            return redirect()->back()->with('validacao', $validacao);
            
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // Verifica se o equipamento possui movimento de entrada e/ou saída
        $entrada = Movimento::where('mov_tipo', '=', 'true')->where(function ($query) use($id)
        {
            $query->where('mov_sn', '=', $id)->orWhere('mov_iccid', '=', $id);
        })->get();

        $saida = Movimento::where('mov_tipo', '=', 'false')->where(function ($query) use($id)
        {
            $query->where('mov_sn', '=', $id)->orWhere('mov_iccid', '=', $id);
        })->get();

        //print_r(count($entrada));
        //print_r($saida);

    
        // Se o equipamento não possui entrada, retorna -1
        if(count($entrada) == 0) return -1;

        // Se o equipamento possuir apenas movimento de entrada, retorna o equipamento
        if($entrada && count($saida) == 0) return json_encode($entrada);

        // Se o equipamento ja possuir movimento de saída, retorna -2
        if(count($saida) == 0) return -2;

        

        return -1;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
       
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function verificaES($dados)
    {
        // Dados da requisição
        $data = $dados;

        // ID do produto
        $id = $data['mov_pro'];

        // Produto do movimento
        $produto = Produto::find($id);

        // ICCID
        $iccid = '';
        if(array_key_exists('mov_iccid', $data)) $iccid = $data['mov_iccid'];
        
        // Numero de Serie
        $sn = '';
        if(array_key_exists('mov_sn', $data))  $sn = $data['mov_sn'];

        // Numero do kit
        $numkit = '';
        if(array_key_exists('mov_numkit', $data))  $numkit = $data['mov_numkit'];
        
        // Verifica se o movimento é de entrada ou saída
        if($data['mov_tipo'] == 'entnov' || $data['mov_tipo'] == 'entdev')
        {   
            // Verifica se é uma devolução e seta o valor de produto novo para negativo
            if($data['mov_tipo'] == 'entdev') $data['mov_novo'] = false;

            // Verifica se o tipo do produto do movimento é kit
            if($produto['pro_tipo'] == 'kit')
            {
            // Se for kit, verifica se existem entradas para o chip e o equipamento repassados

                // Verifica se é uma devolução e seta o valor de produto novo para negativo
                if($data['mov_tipo'] == 'entdev')
                {
                    // Retorna para a página de cadastro com a mensagem de validacao
                    return 'Não é possival realizar devolução de KIT';
                }
                
                // Verificacao de Entrada de Chip
                $entradaC = Movimento::where('mov_tipo', '=', 'entnov')->where('mov_iccid', '=', $iccid)->get();
                
                // Senao existe entrada de chip retorna erro  
                if(count($entradaC) == 0)
                {
                    // Retorna para a página de cadastro com a mensagem de validacao
                    return 'Não existe movimentação de entrada para o chip com ICCID: '.$iccid.'!';
                    
                }
                // Verificacao de Saída de Chip
                $saidaC = Movimento::where('mov_tipo', '=', 'saiven')->where('mov_iccid', '=', $iccid)->get();
                // Se ja existe movimentacao de saida, informa o erro 
                if(!(count($saidaC) == 0))
                {

                    // Retorna para a página de cadastro com a mensagem de validacao
                    return 'Já existe movimentação de saída para o chip com ICCID: '.$iccid.'!';
                    
                }

                // Verificacao de Entrada de Equipamento
                $entradaE = Movimento::where('mov_tipo', '=', 'entnov')->where('mov_sn', '=', $sn)->get();
                
                // Senao existe entrada de Equipamento retorna erro  
                if(count($entradaE) == 0)
                {
                    // Retorna para a página de cadastro com a mensagem de validacao
                    return 'Não existe movimentação de entrada para o Equipamento com SN: '.$sn.'!';
                    
                }
                // Verificacao de Saída de Equipamento
                $saidaE = Movimento::where('mov_tipo', '=', 'saiven')->where('mov_sn', '=', $sn)->get();
                // Se ja existe movimentacao de saida, informa o erro  
                if(!(count($saidaE) == 0))
                {
                    // Retorna para a página de cadastro com a mensagem de validacao
                    return 'Já existe movimentação de saída para o Equipamento com SN: '.$sn.'!';
                    
                }
            // Senao
                // Cria movimento de saída de chip e de equipamento
                $saidaC = $entradaC;
                // Altera o valor para saida
                $saidaC[0]->mov_tipo = 'saiven';
                // Da saida no chip
                Movimento::create($saidaC[0]->toArray());
                // Cria movimento de saída de chip e de equipamento
                $saidaE = $entradaE;
                // Altera o valor para saida
                $saidaE[0]->mov_tipo = 'saiven';

                // Geração do Número do Kit
                $nextId = DB::table('kits')->max('id') + 1;
                $numCasas = round($nextId / 10)+1;
                $kit_num = '075550318';
                for($i=0; $i<=(7-$numCasas); $i++)
                {
                    $kit_num .= '0';
                }
                $kit_num .= $nextId;

                // Da saida no equipamento
                Movimento::create($saidaE[0]->toArray());

                // Adiciona o número gerado ao kit
                $data['mov_numkit'] = $kit_num;
               

                // Da entrada no movimento do KIT
                Movimento::create($data);

                // Retorna para a página de cadastro com a mensagem de cadastro
                return 'Movimento de entrada do produto: '.$produto['pro_dsc'].' processada com sucesso!';
                
            }


           // Se for entrada e o produto nao for kit, verifica se já nao existe entrada com os valores

           //Verifica se é devolucao
           if($data['mov_tipo'] == 'entdev')
           {

                // Verifica se o produto possui saida como comodato
                $saida = Movimento::where('mov_tipo', '=', 'saicom')->where(function ($query) use($iccid, $sn)
                {
                    $query->where('mov_sn', '=', $sn)->orWhere('mov_iccid', '=', $iccid);
                })->get();

                
                // Se o equipamento não possui entrada, 
                if(!(count($saida) == 0)){

                    // Cria o movimento
                    Movimento::create($data);

                    // Retorna para a página de cadastro com a mensagem de cadastro
                    return 'Movimento de entrada de devolução do produto: '.$produto['pro_dsc'].' processada com sucesso!';
                    
                }
               
               // Retorna para a página de cadastro com a mensagem de cadastro
               return 'Já existe movimentação de entrada de devolução com as informações repassadas, verifique e tente novamente!';
           }


           // Senao
           $entrada = Movimento::where('mov_tipo', '=', 'entnov')->where(function ($query) use($iccid, $sn)
            {
                $query->where('mov_sn', '=', $sn)->orWhere('mov_iccid', '=', $iccid);
            })->get();

            
            // Se o equipamento não possui entrada, 
            if(count($entrada) == 0){

                // Cria o movimento
                Movimento::create($data);

                // Retorna para a página de cadastro com a mensagem de cadastro
                return 'Movimento de entrada do produto: '.$produto['pro_dsc'].' processada com sucesso!';
                
            }

            // Retorna para a página de cadastro com a mensagem de cadastro
            return 'Já existe movimentação de entrada com as informações repassadas, verifique e tente novamente!';
            
        }


        // Se for saída, verifica se ja existe a saida e se existe entrada correspondente
        if($data['mov_tipo'] == 'saiven' || $data['mov_tipo'] == 'saicom')
        {

            // Verifica se o produto é um kit
            if($produto['pro_tipo'] == 'kit')
            {
                // Verifica se o produto possui movimento de entrada e/ou saída
                $entrada = Movimento::where('mov_tipo', '=', 'entnov')->where('mov_numkit', '=', $numkit)->get();

                $saida = Movimento::where('mov_tipo', '=', 'saiven')->where('mov_numkit', '=', $numkit)->get();
            }
            // Senao
            if(!($produto['pro_tipo'] == 'kit'))
            {
                // Verifica se o produto possui movimento de entrada e/ou saída
                $entrada = Movimento::where('mov_tipo', '=', 'entnov')->where(function ($query) use($iccid, $sn)
                {
                    $query->where('mov_sn', '=', $sn)->orWhere('mov_iccid', '=', $iccid);
                })->get();

                $saida = Movimento::where('mov_tipo', '=', 'saiven')->where(function ($query) use($iccid, $sn)
                {
                    $query->where('mov_sn', '=', $sn)->orWhere('mov_iccid', '=', $iccid);
                })->get();

                // Verifica se é acessório
                if($produto['pro_tipo'] == 'ace')
                {
                    $entrada = Movimento::where('mov_tipo', '=', 'entnov')->where('mov_pro', '=', $produto['id'])->get();
    
                    $saida = Movimento::where('mov_tipo', '=', 'saiven')->where('mov_pro', '=', $produto['id'])->get();

                    
                    // Se o equipamento não possui entrada
                    if(count($entrada) == 0)
                    {
                        // Retorna para a página de cadastro com a mensagem de validacao
                        return 'Não existe movimentação de entrada para o produto com os dados informados, verifique e tente novamente!';
                        
                    }
                            
                    // Verifica se o número de entradas é maior que de saída para poder da saída

                    if(count($entrada) > count($saida))
                    {
                        // Cria o movimento de saida
                        Movimento::create($data);

                        // Retorna para a página de cadastro com a mensagem de validacao
                        return 'Movimentação de saida do produto: '.$produto['pro_dsc'].', processada com sucesso!';    
                    }

                    return 'Não existem movimentações de entrada suficientes para saída solicitada!';


                }

            }
            

            // Se o equipamento não possui entrada
            if(count($entrada) == 0)
            {
                // Retorna para a página de cadastro com a mensagem de validacao
                return 'Não existe movimentação de entrada para o produto com os dados informados, verifique e tente novamente!';
                
            }

            // Se possui entrada e nao possuir saida
            if($entrada && count($saida) == 0)
            {
                // Cria o movimento de saida
                Movimento::create($data);

                // Retorna para a página de cadastro com a mensagem de validacao
                return 'Movimentação de saida do produto: '.$produto['pro_dsc'].', processada com sucesso!';
                
            }

            // Se ja possuir saida  
            if($saida)
            {
                  // Retorna para a página de cadastro com a mensagem de validacao
                  return 'Já existe movimentação de saída para o produto com os dados informados, verifique e tente novamente!';
                  
            }
        }
    }
    // Função para verificar se existem produtos com estoque próximo ou iguais ao estoque minimo
    public function verificaEstoqueMin()
    {
        //  Produtos cadastrados no banco 
        $produtosSQ = Produto::all();
        
        // Array que armazena os produtos juntamente com suas quantidades em estoque
        $produtosQ = [];
 
        // Itera sobre os produtos cadastrados no banco
        foreach($produtosSQ as $prod)
        {
            // Captura o id do produto
            $id = $prod['id'];
            // Chamada de função do BD que calcula a quantidade de estoque do produto informado
            $qtd = DB::select('SELECT public."quantidadeestoque"('.$id.')');
            // Adiciona a quantidade ao produto
            $prod->qtd = $qtd[0]->quantidadeestoque;

            // Verifica se o estoque é minimo ou próximo do minimo
            // || $prod->qtd < ($prod->pro_min + 2)
            if($prod->qtd <= $prod->pro_min)
            {
                // Adiciona o produto ao array que será retornada a view
                array_push($produtosQ, $prod);
            }
            
        }

        if(count($produtosQ) > 0) Mail::to('engenharia.edson@ranor.com.br')->queue(new AvisoEstoqueMin($produtosQ));

        
    }
}
