<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use App\User;

class UsuarioController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // Busca do banco todos os usuários para carregar na view
        $usuarios = User::get();

        // Busca na sessão os dados do usuario logado
        $usuarioLogado = auth()->user();
        
        // Funcao da biblioteca spatie/laravel que busca as roles de um usuario
        $rolesUsuario = $usuarioLogado->roles;

        // Funcao da biblioteca spatie/laravel que busca as permissoes de um usuario
        $permissionsUsuario = $usuarioLogado->permissions;

        // Funcao da biblioteca spatie/laravel que busca as permissoes de um usuario
        $roles = Role::get();

        // Variável responsável por passar ao breadcrumb(lista de migalhas)
        // o caminho percorrido
        $listaMigalhas = json_encode([
            ['titulo' => 'Home', 'url' => '/'],
            ['titulo' => 'Usuarios', 'url' => '']
        ]);



        return view('admin.usuario.index', compact('usuarios', 'listaMigalhas', 'rolesUsuario', 'permissionsUsuario', 'roles'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // Pega todos os dados da requisao
        $data = $request->all();

        // Busca usuario para modificar
        $usuario = User::find($id);

        // Verifica se possui a variavel com as roles
        if(isset($data['roles'])) {
            // Armazena as roles em uma variável
            $roles = $data['roles'];
            

            // Modifica as roles do usuario, funcao do spatie que remove todas as roles
            // e atualiza com as roles do array passado como parametro.
            $usuario->syncRoles($roles);

        }
        
        // Atualiza o usuario com as informações 
        $usuario->update($data);

        // Busca todos os usuarios no banco
        $listaUsuarios = User::all();

        // Retorna os usuarios  
        return $listaUsuarios;

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // Busca o usuario a ser excluido
        $usuario = User::find($id);

        // Deleta o usuario

        $usuario->delete();

        // Retorna todos os usuarios
        return User::all();
    }
}
