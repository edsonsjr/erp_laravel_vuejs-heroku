<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use App\Http\Controllers\Controller;
use App\User;

class UsuarioRoleController extends Controller
{
    //
    public function buscaRolesUsuario($idUsuario)
    {
        // Busca no banco o usuario com id passado por parametro
        $usuario = User::find($idUsuario);

        // Busca quais os nomes das funcoes atreladas a esse usuario
        $rolesUsuario = $usuario->getRoleNames();

        // Retorna as funcoes  
        return $rolesUsuario;
    }
}
