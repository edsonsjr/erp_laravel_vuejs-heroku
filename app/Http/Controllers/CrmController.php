<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class CrmController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     * $this->middleware('auth');
     */
    public function index()
    {
        //
        $listaOpcoesEmpresa = json_encode([
            ["titulo"=>"Contas", 
            "acoes"=>[["icone"=>"fa fa-plus", "texto"=>"Incluir", "url"=>"/add"],
                ["icone"=>"fa fa-list-ul", "texto"=>"Exibir Todas", "url"=>"/show"],]
            ],
            ["titulo"=>"Contas a receber", 
            "acoes"=>[["icone"=>"fa fa-plus", "texto"=>"Incluir", "url"=>"/add"],
                ["icone"=>"fa fa-list-ul", "texto"=>"Exibir Todas", "url"=>"/show"],
                ["icone"=>"fa fa-plus", "texto"=>"Deletar", "url"=>"/add"],
                ["icone"=>"fa fa-list-ul", "texto"=>"Ação Todas", "url"=>"/show"],
                ["icone"=>"fa fa-plus", "texto"=>"Ação Qualquer", "url"=>"/add"],
                ["icone"=>"fa fa-list-ul", "texto"=>"Exibir", "url"=>"/show"],]
            ],
            ["titulo"=>"Contas", 
            "acoes"=>[["icone"=>"fa fa-plus", "texto"=>"Incluir", "url"=>"/add"],
                ["icone"=>"fa fa-list-ul", "texto"=>"Exibir Todas", "url"=>"/show"]]
            ],
            ["titulo"=>"Contas", 
            "acoes"=>[["icone"=>"fa fa-plus", "texto"=>"Incluir", "url"=>"/add"],
                ["icone"=>"fa fa-list-ul", "texto"=>"Exibir Todas", "url"=>"/show"]]
            ]
        ]);

        return view('modulos/crm/home', compact('listaOpcoesEmpresa'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
