<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Movimento extends Model
{
    protected $fillable = ['mov_numkit','mov_sn', 'mov_iccid', 'mov_numchip', 'mov_for','mov_sit', 'mov_obs', 'mov_novo', 'mov_tipo', 'mov_mb'
    , 'mov_diavenc', 'mov_mens', 'mov_val', 'mov_pro'];

    protected $hidden = ['created_at', 'updated_at'];
    //
    public function produto()
    {
        return $this->belongsTo('App\Produto');
    }
}
