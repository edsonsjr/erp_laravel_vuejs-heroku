<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kit extends Model
{
    //

    protected $fillable = ['kit_num', 'kit_nome', 'id_chip', 'id_equip'];

    public function chip()
    {
        return $this->hasOne('App\Chip');
    }

    public function equipamento()
    {
        return $this->hasOne('App\Equipamento');
    }
}
